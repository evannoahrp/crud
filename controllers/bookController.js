const { books } = require("../models");

module.exports = {
  // create buku
  create: (req, res) => {
    const { isbn, judul, sinopsis, penulis, genre } = req.body;
    books
      .create({
        isbn,
        judul,
        sinopsis,
        penulis,
        genre,
      })
      .then((book) => {
        res.json({
          status: 200,
          message: "Berhasil",
          data: book,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  // read semua buku
  index: (req, res) => {
    books
      .findAll()
      .then((book) => {
        if (book !== 0) {
          res.json({
            status: 200,
            message: "Berhasil",
            data: book,
          });
        } else {
          res.json({
            status: 400,
            message: "Kosong",
          });
        }
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  // read buku by id
  show: (req, res) => {
    const bookId = req.params.id;
    books
      .findOne({
        where: {
          id: bookId,
        },
      })
      .then((book) => {
        res.json({
          status: 200,
          message: "Berhasil",
          data: book,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  // update buku by id
  update: (req, res) => {
    const bookId = req.params.id;
    const { isbn, judul, sinopsis, penulis, genre } = req.body;
    books
      .update(
        {
          isbn,
          judul,
          sinopsis,
          penulis,
          genre,
        },
        {
          where: { id: bookId },
        }
      )
      .then((book) => {
        res.json({
          status: 201,
          data: book,
        });
      })
      .catch((err) => {
        res.json({
          status: 422,
          message: "Gak bisa update buku",
        });
      });
  },

  // delete buku by id
  delete: (req, res) => {
    const bookId = req.params.id;
    books
      .destroy({
        where: {
          id: bookId,
        },
      })
      .then(() => {
        res.json({
          status: 200,
          message: "Buku dihapus",
        });
      });
  },
};
