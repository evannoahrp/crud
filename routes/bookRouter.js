const router = require("express").Router();
const bookController = require("../controllers/bookController");

router.get("/", bookController.index);
router.get("/:id", bookController.show);
router.post("/create", bookController.create);
router.patch("/:id", bookController.update);
router.delete("/:id", bookController.delete);

module.exports = router;
