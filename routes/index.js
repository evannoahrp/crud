const router = require("express").Router();

const bookRouter = require("./bookRouter");

router.use("/books", bookRouter);

module.exports = router;
